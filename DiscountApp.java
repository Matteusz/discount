import java.util.Scanner;

public class DiscountApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Client client1 = new Client("Jan", "Kowalski", true);
        System.out.print("Podaj cenę: ");
        double price = scanner.nextDouble();
        DiscountService discountService = new DiscountService();
        double discountPrice = discountService.calculateDiscountPrice(client1, price);

        System.out.println("Witaj " + client1.getName() + " " + client1.getSurname());
        System.out.println("Kwota przed rabatem: " + price);
        System.out.println("Do zapłaty po rabacie: " + discountPrice);

        Client client2 = new Client("Michał", "Prokop", false);
        System.out.println("Podaj cenę: ");
        double price2 = scanner.nextDouble();
        double discountPrice2 = discountService.calculateDiscountPrice(client2, price2);

        System.out.println("Witaj " + client2.getName() + " " + client2.getSurname());
        System.out.println("Kwota przed rabatem: " + price2);
        System.out.println("Do zapłaty po rabacie: " + discountPrice2);




    }
}
