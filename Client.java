public class Client {
    private String name;
    private String surname;
    private boolean clientPreium;

    public Client(String name, String surname, boolean clientPreium) {
        this.name = name;
        this.surname = surname;
        this.clientPreium = clientPreium;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isClientPreium() {
        return clientPreium;
    }

    public void setClientPreium(boolean clientPreium) {
        this.clientPreium = clientPreium;
    }
}
